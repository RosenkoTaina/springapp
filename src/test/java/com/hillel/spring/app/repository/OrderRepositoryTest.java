package com.hillel.spring.app.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import com.hillel.spring.app.entity.Order;
import com.hillel.spring.app.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository repository;

    @Test
    void getAllTest() {

        List<Order> Orders = repository.findAll();

        assertTrue(Orders.size() > 2);

    }

    @Test
    void getOrderById() {

        Optional<Order> orders = repository.findById(5L);

        assertEquals(5, orders.get().getId());
        assertEquals("2022-02-02 00:00:00.0", orders.get().getDate().toString());
        assertEquals(76, orders.get().getCost());


    }

}
