package com.hillel.spring.app.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import com.hillel.spring.app.dto.ProductDto;
import com.hillel.spring.app.mapper.ProductMapper;
import com.hillel.spring.app.repository.ProductRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class ProductService {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    public List<ProductDto> getAll() {
        log.info("Getting all products");

        return mapper.toDto(repository.findAll());
    }

    public ProductDto getById(Long id) {
        log.info("Getting Product by id: {}", id);
        return mapper.toDto(repository.getReferenceById(id));
    }

    public ProductDto create(ProductDto product) {
        product.setId(null);
        log.info("Creating Product: {}", product);
        return mapper.toDto(repository.save(mapper.toEntity(product)));
    }

    public ProductDto update(ProductDto Product) {
        log.info("Updating Product: {}", Product);
        return mapper.toDto(repository.save(mapper.toEntity(Product)));
    }

    public void delete(Long id) {
        log.info("Deleting Product by Id: {}", id);
        repository.deleteById(id);
    }

}
