package com.hillel.spring.app.service;

import java.util.List;

import com.hillel.spring.app.dto.ProductDto;
import com.hillel.spring.app.entity.Order;
import com.hillel.spring.app.entity.OrderKey;
import com.hillel.spring.app.entity.Product;
import com.hillel.spring.app.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import com.hillel.spring.app.dto.OrderDto;
import com.hillel.spring.app.mapper.OrderMapper;
import com.hillel.spring.app.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class OrderService {

    private final OrderRepository repository;
    private final OrderMapper mapper;
    @Autowired
    private ProductRepository productRepository;

    public List<OrderDto> getAll() {
        log.info("Getting all orders");
        return mapper.toDto(repository.findAll());
    }

    public OrderDto getById(Long id, Long productId) {
        log.info("Getting Order by id: {}", id);
        OrderKey orderKey = new OrderKey(id, productId);
        return mapper.toDto(repository.findById(orderKey).orElse(null));
    }

    public OrderDto create(OrderDto orderDto) {
        log.info("Creating Order: {}", orderDto);
        Order order = mapper.toEntity(orderDto);

        if (orderDto.getId() == null) {
            order.setId(repository.findAll().size() + 1L);
        }

        if (orderDto.getProductId() != null) {
            Product product = productRepository.findById(orderDto.getProductId()).orElseThrow(() -> new IllegalArgumentException("Product not found with ID: " + orderDto.getProductId()));
            order.setProductId(product.getId());
        }

        Order savedOrder = repository.save(order);
        OrderDto savedOrderDto = mapper.toDto(savedOrder);

        return savedOrderDto;
    }

    public OrderDto update(OrderDto Order) {
        log.info("Updating Order: {}", Order);
        return mapper.toDto(repository.save(mapper.toEntity(Order)));
    }


    public OrderDto updateById(OrderDto Order, Long id, Long productId) {
        log.info("Updating Order with ID: {} and Product ID: {}", id, productId);
        Order.setId(id);
        Order.setProductId(productId);
        return mapper.toDto(repository.save(mapper.toEntity(Order)));
    }

    public void delete(Long id, Long productId) {
        log.info("Deleting Order by Id: {} and ProductId {}", id, productId);
        OrderKey orderKey = new OrderKey(id, productId);
        repository.deleteById(orderKey);
    }
}
