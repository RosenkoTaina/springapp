package com.hillel.spring.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/ping")
public class PingController {

    @GetMapping
    public ResponseEntity statusCheck() {
        return ResponseEntity.ok("OK");
    }

}
