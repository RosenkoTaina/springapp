package com.hillel.spring.app.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import com.hillel.spring.app.dto.OrderDto;
import com.hillel.spring.app.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService service;

    @GetMapping
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}/{productId}")
    public ResponseEntity<OrderDto> getById(@PathVariable("id") Long id, @PathVariable("productId") Long productId) {
        return ResponseEntity.ok(service.getById(id, productId));
    }

    @PostMapping
    public ResponseEntity<OrderDto> create(@RequestBody OrderDto dto) {
        return ResponseEntity.ok(service.create(dto));
    }

    @PutMapping
    public ResponseEntity<OrderDto> update(@RequestBody OrderDto dto) {
        return ResponseEntity.ok(service.update(dto));
    }

    @PutMapping("/{id}/{productId}")
    public ResponseEntity<OrderDto> updateById(@PathVariable("id") Long id, @PathVariable("productId") Long productId, @RequestBody OrderDto dto) {
        return ResponseEntity.ok(service.updateById(dto, id, productId));
    }

    @DeleteMapping("/{id}/{productId}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @PathVariable("productId") Long productId) {
        service.delete(id, productId);
        return ResponseEntity.ok().build();
    }
}


