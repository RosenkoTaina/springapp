package com.hillel.spring.app.mapper;

import java.util.List;

import com.hillel.spring.app.dto.ProductDto;
import com.hillel.spring.app.entity.Product;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product toEntity(ProductDto dto);

    ProductDto toDto(Product Product);

    List<Product> toEntity(List<ProductDto> dtos);

    List<ProductDto> toDto(List<Product> entities);
}
