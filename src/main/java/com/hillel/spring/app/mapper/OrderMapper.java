package com.hillel.spring.app.mapper;

import java.util.List;

import com.hillel.spring.app.dto.OrderDto;
import com.hillel.spring.app.entity.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    Order toEntity(OrderDto dto);

    OrderDto toDto(Order Order);

    List<Order> toEntity(List<OrderDto> dtos);

    List<OrderDto> toDto(List<Order> entities);
}
