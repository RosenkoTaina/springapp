package com.hillel.spring.app.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "orders")
@IdClass(OrderKey.class)
public class Order implements Serializable {

    @Id
    private Long id;
    private Date date;
    private int cost;

    @Id
    @Column(name = "product_id")
    private Long productId;
}

