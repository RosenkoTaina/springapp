package com.hillel.spring.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderKey implements Serializable {
    private Long id;
    private Long productId;
}
