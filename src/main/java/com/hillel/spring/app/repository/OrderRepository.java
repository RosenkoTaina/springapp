package com.hillel.spring.app.repository;

import com.hillel.spring.app.entity.Order;
import com.hillel.spring.app.entity.OrderKey;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository extends JpaRepository<Order, OrderKey> {

}