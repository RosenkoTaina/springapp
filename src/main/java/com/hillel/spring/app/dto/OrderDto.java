package com.hillel.spring.app.dto;

import lombok.Data;
import java.util.Date;

@Data
public class OrderDto {

    private Long id;
    private Date date;
    private int cost;
    private Long productId;

}
