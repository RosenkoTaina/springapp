package com.hillel.spring.app.dto;

import lombok.Data;

@Data
public class ProductDto {

    private Long id;
    private String name;
    private int cost;
}
